#include "Mbox.h"

Mbox::Mbox()
{
	soundEngine = createIrrKlangDevice();
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);

	glBindVertexArray(VAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GL_FLOAT), (GLvoid*)0);
	glEnableVertexAttribArray(0);

	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GL_FLOAT), (GLvoid*)(3 * sizeof(GL_FLOAT)));
	glEnableVertexAttribArray(2);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void Mbox::createBox(glm::vec3 center, GLint shaderProgram)
{
	if (isOpen) 
	{
		if (openAngle < 135.0f) openAngle += 0.5f;
		if (openAngle == 134.0f) soundEngine->play2D("sample.mp3", GL_TRUE);
	}
	else 
	{
		if (openAngle > 0) openAngle-= 0.5f;
		if (openAngle == 1.0f) soundEngine->setAllSoundsPaused();
	}

	//std::cout << openAngle << std::endl;

	this->center = center;
	glBindVertexArray(VAO);

	// right one
	glm::mat4 model = glm::mat4(1.0f);
	model = glm::translate(model, center + glm::vec3(1.45f, 0.55f, 0.0f));
	model = glm::scale(model, glm::vec3(0.1f, 1.0f, 2.0f));
	GLuint transformLoc1 = glGetUniformLocation(shaderProgram, "model");
	glUniformMatrix4fv(transformLoc1, 1, GL_FALSE, glm::value_ptr(model));
	glDrawArrays(GL_TRIANGLES, 0, 36);
	
	// left one
	model = glm::mat4(1.0f);
	model = glm::translate(model, center + glm::vec3(-1.45f, 0.55f, 0.0f));
	model = glm::scale(model, glm::vec3(0.1f, 1.0f, 2.0f));
	transformLoc1 = glGetUniformLocation(shaderProgram, "model");
	glUniformMatrix4fv(transformLoc1, 1, GL_FALSE, glm::value_ptr(model));
	glDrawArrays(GL_TRIANGLES, 0, 36);

	// top one
	model = glm::mat4(1.0f);
	model = glm::translate(model, center + glm::vec3(0.0f, 1.1f, -1.0f));
	model = glm::rotate(model, glm::radians(-openAngle), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::translate(model, glm::vec3(0.0f, 0.0f, 1.0f));
	model = glm::scale(model, glm::vec3(3.0f, 0.10f, 2.0f));
	transformLoc1 = glGetUniformLocation(shaderProgram, "model");
	glUniformMatrix4fv(transformLoc1, 1, GL_FALSE, glm::value_ptr(model));
	glDrawArrays(GL_TRIANGLES, 0, 36);

	// bottom one
	model = glm::mat4(1.0f);
	model = glm::translate(model, center + glm::vec3(0.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(3.0f, 0.10f, 2.0f));
	transformLoc1 = glGetUniformLocation(shaderProgram, "model");
	glUniformMatrix4fv(transformLoc1, 1, GL_FALSE, glm::value_ptr(model));
	glDrawArrays(GL_TRIANGLES, 0, 36);

	// front one
	model = glm::mat4(1.0f);
	model = glm::translate(model, center + glm::vec3(0.0f, 0.55f, 0.95f));
	model = glm::scale(model, glm::vec3(2.8f, 1.0f, 0.1f));
	transformLoc1 = glGetUniformLocation(shaderProgram, "model");
	glUniformMatrix4fv(transformLoc1, 1, GL_FALSE, glm::value_ptr(model));
	glDrawArrays(GL_TRIANGLES, 0, 36);

	// back one
	model = glm::mat4(1.0f);
	model = glm::translate(model, center + glm::vec3(0.0f, 0.55f, -0.95f));
	model = glm::scale(model, glm::vec3(2.8f, 1.0f, 0.1f));
	transformLoc1 = glGetUniformLocation(shaderProgram, "model");
	glUniformMatrix4fv(transformLoc1, 1, GL_FALSE, glm::value_ptr(model));
	glDrawArrays(GL_TRIANGLES, 0, 36);

	glBindVertexArray(0);
}


Mbox::~Mbox()
{
}
