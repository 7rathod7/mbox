#include <iostream>

#define GLEW_STATIC
#define GLM_ENABLE_EXPERIMENTAL
#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include "Shader.h"
#include "Camera.h"
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <glm\ext.hpp>
#include <glm/gtx/string_cast.hpp>
#include "Mbox.h"
#include "Texture.h"

using namespace std;

bool keys[1024];

// time between two frames
GLfloat lastFrame = 0.0f;	// last frame time

GLfloat lastX = 400, lastY = 300;
GLfloat deltaTime = 0.0f;
bool firstMouse = true;

Camera camera(glm::vec3(0.0f, 2.0f, 5.0f));
Mbox *mbox1 = NULL, *mbox2 = NULL;

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode) {
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
	if (key == GLFW_KEY_O && action == GLFW_PRESS)
		mbox1->isOpen = GL_TRUE;
	if (key == GLFW_KEY_C && action == GLFW_PRESS)
		mbox1->isOpen = GL_FALSE;
	if (key >= 0 && key < 1024)
	{
		if (action == GLFW_PRESS)
			keys[key] = true;
		else if (action == GLFW_RELEASE)
			keys[key] = false;
	}

}

void mouse_callback(GLFWwindow* window, double xpos, double ypos) {
	if (firstMouse) {
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	GLfloat xoffset = xpos - lastX;
	GLfloat yoffset = lastY - ypos; // reversed since y coordinate range from bottom to top
	lastX = xpos;
	lastY = ypos;

	camera.ProcessMouseMovement(xoffset, yoffset);	
}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
	camera.ProcessMouseScroll(yoffset);
}

void doMovement() {
	// Camera controls
	if (keys[GLFW_KEY_W])
		camera.ProcessKeyboard(FORWARD, deltaTime);
	if (keys[GLFW_KEY_S])
		camera.ProcessKeyboard(BACKWARD, deltaTime);
	if (keys[GLFW_KEY_A])
		camera.ProcessKeyboard(LEFT, deltaTime);
	if (keys[GLFW_KEY_D])
		camera.ProcessKeyboard(RIGHT, deltaTime);
}

void printVersion()
{
	const GLubyte *renderer = glGetString(GL_RENDERER);
	const GLubyte *version = glGetString(GL_VERSION);
	GLint nrVertexAttributes;
	glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nrVertexAttributes);
	cout << "Maximum number of vertex attributes are supported -> " << nrVertexAttributes << "\n";

	cout << "renderer -> " << renderer << "\n";
	cout << "version -> " << version << "\n";
}

int main() {
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	GLFWwindow* window = glfwCreateWindow(800, 600, "shader", nullptr, nullptr);

	if (window == nullptr) {
		cout << "failed to create glfw window\n";
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);
	glfwSetKeyCallback(window, key_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);

	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	glewExperimental = GL_TRUE;

	if (glewInit() != GLEW_OK) {
		cout << "failed to initialise glew\n";
		return -1;
	}

	printVersion();

	glViewport(0, 0, 800, 600);

	// setup opengl options
	glEnable(GL_DEPTH_TEST);

	mbox1 = new Mbox();
	mbox2 = new Mbox();

	// generating texture
	GLuint texture = getTexture();

	Shader shaderProgram = Shader("vertex.glsl", "fragment.glsl");

	while (!glfwWindowShouldClose(window)) {
		GLfloat currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;
		
		glfwPollEvents();
		doMovement();

		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		shaderProgram.Use();

		glm::mat4 view = camera.GetViewMatrix();

		glm::mat4 projection = glm::mat4(1.0f);
		projection = glm::perspective(glm::radians(camera.Zoom), 800.0f / 600.0f, 0.1f, 100.0f);

		// binding texture
		glBindTexture(GL_TEXTURE_2D, texture);

		GLuint transformLoc2 = glGetUniformLocation(shaderProgram.Program, "view");
		glUniformMatrix4fv(transformLoc2, 1, GL_FALSE, glm::value_ptr(view));

		GLuint transformLoc3 = glGetUniformLocation(shaderProgram.Program, "projection");
		glUniformMatrix4fv(transformLoc3, 1, GL_FALSE, glm::value_ptr(projection));

		mbox1->createBox(glm::vec3(0.0f, 0.0f, 0.0f), shaderProgram.Program);
		mbox2->createBox(glm::vec3(5.0f, 0.0f, 0.0f), shaderProgram.Program);
		glfwSwapBuffers(window);
	}

	/*glDeleteVertexArrays(1, &mbox1->VAO);
	glDeleteBuffers(1, &mbox1->VBO);
	glDeleteVertexArrays(1, &mbox2->VAO);
	glDeleteBuffers(1, &mbox2->VBO);*/
	glfwTerminate();
	return 0;
}